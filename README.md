# Features
Adds an extra window that will show you what a settlement will sell to you. Without having to go all the way there to check.

It has options to require a first visit to the settlement or to have a comms console before it can be viewed. These can be found in the mod settings menu.

Note: Opening the window counts as a visit to the settlement, which will cause the stock refresh timer to start.

# Languages
* English
* Chinese Simplified (Thanks to [HawnHan](https://steamcommunity.com/id/HawnHan))
* Dutch

Feel free to send me a translation file for any other languages and I'll gladly add it.

# License
This mod is licensed under the [MIT license](https://tldrlegal.com/license/mit-license).

# Versions
## 1.2.5
* Updated to RimWorld v1.5

## 1.2.4
* Updated to RimWorld v1.4

## 1.2.3
* Updated to RimWorld v1.3

## 1.2.2
* Updated to RimWorld v1.2

## 1.2.1
* Fixed default settings (Require first visit should be on by default)
* Updated Chinese Simplified localization

## 1.2
* Made first visit requirement optional
* Added optional comms console requirement

## 1.1
* The window now also shows the animals that the trader will sell

## 1.0.1
* Added Chinese Simplified localization (Thanks to [HawnHan](https://steamcommunity.com/id/HawnHan))
* Added Dutch localization

## 1.0
* Initial release