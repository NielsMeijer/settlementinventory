﻿using Verse;

namespace NMeijer.SettlementInventory
{
	public class SettlementInventorySettings : ModSettings
	{
		#region Variables

		public bool RequireFirstVisit = true;
		public bool RequireCommsConsole;

		#endregion

		#region Public Methods

		public override void ExposeData()
		{
			Scribe_Values.Look(ref RequireFirstVisit, "settlementInventoryRequireFirstVisit", true);
			Scribe_Values.Look(ref RequireCommsConsole, "settlementInventoryRequireCommsConsole");

			base.ExposeData();
		}

		#endregion
	}
}