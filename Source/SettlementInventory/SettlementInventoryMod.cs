﻿using HarmonyLib;
using UnityEngine;
using Verse;

namespace NMeijer.SettlementInventory
{
	public class SettlementInventoryMod : Mod
	{
		#region Properties

		public static SettlementInventorySettings Settings
		{
			get; private set;
		}

		#endregion

		public SettlementInventoryMod(ModContentPack modContentPack)
			: base(modContentPack)
		{
			Harmony harmony = new Harmony("nl.nmeijer.settlementinventory");
			harmony.PatchAll();

			Settings = GetSettings<SettlementInventorySettings>();
		}

		#region Public Methods

		public override void DoSettingsWindowContents(Rect inRect)
		{
			Listing_Standard listingStandard = new Listing_Standard();
			listingStandard.Begin(inRect);
			listingStandard.CheckboxLabeled("RequireFirstVisit".Translate(), ref Settings.RequireFirstVisit);
			listingStandard.CheckboxLabeled("RequireCommsConsole".Translate(), ref Settings.RequireCommsConsole);
			listingStandard.End();

			base.DoSettingsWindowContents(inRect);
		}

		public override string SettingsCategory()
		{
			return "SettlementInventory".Translate();
		}

		#endregion
	}
}