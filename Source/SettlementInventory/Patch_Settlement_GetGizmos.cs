﻿using System.Collections.Generic;
using HarmonyLib;
using RimWorld.Planet;
using Verse;

namespace NMeijer.SettlementInventory
{
	[HarmonyPatch(typeof(Settlement), nameof(Settlement.GetGizmos))]
	public class Patch_Settlement_GetGizmos
	{
		#region Private Methods

		private static void Postfix(ref IEnumerable<Gizmo> __result, ref Settlement __instance)
		{
			Settlement settlement = __instance;

			if(settlement.Faction.def.permanentEnemy || settlement.Faction.IsPlayer)
			{
				return;
			}

			Command_Action commandAction = new Command_Action
			{
				defaultLabel = "CommandShowBuyableItems".Translate(),
				defaultDesc = "CommandShowBuyableItemsDesc".Translate(),
				icon = Settlement.ShowSellableItemsCommand,
				action = () => Current.Root.uiRoot.windows.Add(new Dialog_BuyableItems(settlement))
			};

			__result = new List<Gizmo>(__result) {commandAction};
		}

		#endregion
	}
}